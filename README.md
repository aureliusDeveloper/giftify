
<img alt='Giftify logo' src='./project-G/src/assets/giftify-logo.png' width='200px'/>

# giftify - browse, share and upload gifs

## Description
***
Giftify is a simple SPA where you can browse, share and upload gifs
## Installation
***

To start the application you have to clone the repository on your computer, go to the folder, containing the `package.json` file and run `npm install` to install all the dependencies that the project needs to become fully functional (that is, if you already have Node.js installed).
After this, you can run the `npm run dev` to start the app in the browser. Enjoy! 😊

```
npm install
npm run dev
```

## Features and Usage
***
Initial view when you launch the app

![landing-page](./project-G/src/assets/README-IMAGES/annonymous-view.png)
***

Login view
![login-view](./project-G/src/assets/README-IMAGES/login-full.png)

Register view
![register-view](./project-G/src/assets/README-IMAGES/register-full.png)

Login phone view

![login-view-phone](./project-G/src/assets/README-IMAGES/login-phone.png)

Register phone view

![register-view-phone](./project-G/src/assets/README-IMAGES/register-phone.png)

Authenticated user home view

![auth-home-full](./project-G/src/assets/README-IMAGES/logged-view.png)


Authenticated user home view phone

![auth-home-phone-view](./project-G/src/assets/README-IMAGES/logged-phone-view.png)

Authenticated user home view dark theme

![auth-home-full-dark](./project-G/src/assets/README-IMAGES/logged-dark-view.png)



## Technologies used
***

- JavaScript
- ReactJS
- HTML5
- CSS3
- GitLab
- ESLint
- Redux
- redux-toolkit
- React router
