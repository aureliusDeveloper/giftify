export const PUBLIC_HEADER_NAV = Object.freeze({
  type: 'button',
  title: {
    login: 'Log In',
    signup: 'Sign Up',
  },
  routes: {
    login:'/login',
    signup:'/register',
  },
});


export const PRIVATE_HEADER_NAV = Object.freeze({
  type: 'button',
  title: {
    home: 'Home',
    favorites: 'Favorites',
    upload: 'Upload',
    something: 'Logout',
  },
  routes: {
    home:'/private',
    favorites:'/private/favorites',
    upload:'/private/upload',
  },
});



export const categories = Object.freeze({
  trending: 'trending',
  sport: 'sport',
  animals: 'animals',
  fun: 'fun',
  love: 'love',
  music: 'music',
  recent: 'recent',
});


export const themeEnum = Object.freeze({
  light: 'light',
  dark: 'dark'
});


