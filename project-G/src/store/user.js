import { createSlice } from '@reduxjs/toolkit';

const initialUserState = { user: null, userData: null };

const userStateSlice = createSlice({
  name: 'user',
  initialState: initialUserState,
  reducers: {
    setUser(state, action) {
      state.user = action.payload;
    },
    setUserData(state, action) {
      state.userData = action.payload;
    }
  }
});



export const userActions = userStateSlice.actions;

export default userStateSlice;