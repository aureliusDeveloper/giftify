import { configureStore } from "@reduxjs/toolkit";
import categoryStateSlice from "./categories";
import themeStateSlice from "./theme";
import userStateSlice from "./user";


// reducers here
const store = configureStore({reducer: {
   user: userStateSlice.reducer,
   theme: themeStateSlice.reducer,
   category: categoryStateSlice.reducer,
}});


export default store;