import { createSlice } from "@reduxjs/toolkit";


const initialCategoryButtonsState = {category: ''};

const categoryStateSlice = createSlice({
    name: 'category',
    initialState: initialCategoryButtonsState,
    reducers: {
        setActive(state, payload) {
            state.category = payload.payload;
        }
    }
});

export const categoryButtonsActions = categoryStateSlice.actions;

export default categoryStateSlice;

