import { createSlice } from '@reduxjs/toolkit';
import { themeEnum } from '../common/enums';

const initialThemeState = {
  theme: localStorage.getItem('theme') || themeEnum.light,
};

const themeStateSlice = createSlice({
  name: 'theme',
  initialState: initialThemeState,
  reducers: {
    toggleTheme(state) {
      if (state.theme === themeEnum.light) {
        state.theme = themeEnum.dark;
        localStorage.setItem('theme', themeEnum.dark);
      } else {
        state.theme = themeEnum.light;
        localStorage.setItem('theme', themeEnum.light);
      }
    },
  },
});

export const themeActions = themeStateSlice.actions;
export default themeStateSlice;
