import { useEffect, useState } from 'react';
import { gf } from '../common/constants';

const useLoadMoreGifs = (page, pageSize) => {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const [trendingGifs, setTrendingGifs] = useState([]);
  const [hasMore, setHasMore] = useState(false);

  useEffect(() => {

    setLoading(true);
    setError(false);
    gf.trending({ offset: page * pageSize, limit: pageSize })
      .then((res) => {

        setTrendingGifs((prevGifs) => {
          return [...new Set([
            ...prevGifs,
            ...res.data,
          ])];
        });
        setHasMore(res.data.length >= 0);
        setLoading(false);
      })
      .catch((e) => {
        setError(true);
        console.log(e.message);
      });
  }, [page]);
  return { trendingGifs, hasMore, loading, error };
};

export default useLoadMoreGifs;
