import React from 'react'
import { useEffect } from 'react';
import { useState } from 'react';
import { gf } from '../common/constants';
import { categories } from '../common/enums';

const useSearchGifs = (query = categories.trending, page, pageSize) => {
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(false);
    const [searchedGifs, setSearchedGifs] = useState([]);
    const [hasMore, setHasMore] = useState(false);
        useEffect(() => {
            setSearchedGifs([]);
        }, [query]);

    useEffect(() => {

        setLoading(true);
        setError(false);
        gf.search(query, { limit: pageSize, offset: page * pageSize })
          .then((res) => {
    
            setSearchedGifs((prevGifs) => {
              return [...new Set([
                ...prevGifs,
                ...res.data,
              ])];
            });
            setHasMore(res.data.length >= 0);
            setLoading(false);
          })
          .catch((e) => {
            setError(true);
            console.log(e.message);
          });
      }, [page, query]);
      return { searchedGifs, hasMore, loading, error };

}

export default useSearchGifs