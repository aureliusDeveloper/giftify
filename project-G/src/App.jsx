import { useEffect } from 'react'
import { Route, Routes } from 'react-router'
import {useAuthState} from 'react-firebase-hooks/auth';
import './App.css'
import { auth } from './config/firebase'
import Login from './views/Login/Login'
import PrivateHome from './views/PrivateHome/PrivateHome'
import Register from './views/Register/Register'
import { useDispatch } from 'react-redux';
import { userActions } from './store/user';
import { useSelector } from 'react-redux';
import { getUserByUID } from './services/users.service';
import PublicLayout from './components/PublicLayout/PublicLayout'
import PublicHome from './views/PublicHome/PublicHome';
import PrivateRoutes from './components/PrivateRoutes/PrivateRoutes';
import PrivateLayout from './components/PrivateLayout/PrivateLayout';
import Favorites from './views/Favorites/Favorites';
import Upload from './views/Upload/Upload';

function App() {
  const [user, loading] = useAuthState(auth);
  const dispatch = useDispatch();
  const currentUser = useSelector((state) => state.user.user);

  useEffect(() => {
    if (user === null) return;
    dispatch(userActions.setUser({email: user.email, uid: user.uid}));

  }, [user]);

  useEffect(() => {
    if (currentUser === null) return;
      getUserByUID(currentUser?.uid).then((user) => {
        dispatch(userActions.setUserData(user));
      }).catch(e => console.log(e.message));
 
  }, [currentUser?.uid]);

  return (
    <div className="App">
      <Routes>
        <Route path={'/'} element={<PublicLayout loading={loading} user={user}/>}>
        <Route index element={<PublicHome />}></Route>
        <Route path={'/login'} element={<Login />}></Route>
        <Route path={'/register'} element={<Register />}></Route>
        </Route>
      <Route element={<PrivateRoutes user={user} loading={loading} />}>
        <Route path={'/private'} element={<PrivateLayout />}>
        <Route index element={<PrivateHome />}></Route>
        <Route path='/private/favorites' element={<Favorites />}></Route>
        <Route path='/private/upload' element={<Upload />}></Route>
        </Route>
      </Route>
      </Routes>
    </div>
  )
}

export default App
