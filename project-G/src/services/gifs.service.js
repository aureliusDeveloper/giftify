import { API_KEY, gf } from '../common/constants';
import { db } from '../config/firebase';
import {
  get,
  set,
  ref,
  query,
  equalTo,
  orderByChild,
  update,
  push,
} from 'firebase/database';
export const getGifsCategories = async (query) => {
  const response = await gf.search(query, { limit: 10, offset: 10 });

  return response.data;
};

export const addFavoriteGif = async (gifId, username) => {
  return await update(ref(db), {
    [`favorites/${username}/${gifId}`]: true,
  });
};

export const removeFavoriteGif = async (gifId, username) => {
  return await update(ref(db), {
    [`favorites/${username}/${gifId}`]: null,
  });
};

export const getUserFavorites = async (username) => {
  const snapshot = await get(ref(db, `favorites/${username}`));

  if (snapshot.exists()) {
    return Object.keys(snapshot.val());
  }

  return [];
};

export const getFavoriteGif = async (gifId, username) => {
  const snapshot = await get(ref(db, `favorites/${username}/${gifId}`));

  if (snapshot.exists()) {
    return gifId;
  }

  return '';
};

export const toggleLike = async (id, username) => {
  const favorites = await getUserFavorites(username);
  const isExisting = favorites.find((gifId) => gifId === id);

  if (!isExisting) {
    addFavoriteGif(id, username);
  } else {
    removeFavoriteGif(id, username);
  }
};
