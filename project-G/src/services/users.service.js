import {
    get,
    set,
    ref,
    query,
    equalTo,
    orderByChild,
    update,
    push,
  } from 'firebase/database';
import { db } from '../config/firebase';


  export const createUser = async (uid, username, email) => {

    const userData = {
      uid,
      username,
      email,
      createdOn: Date.now(),
    }
  
   await set(ref(db, `users/${username}`), userData);
  }

  export const getUserByUID = async (uid) => {

    const snapshot = await get(query(ref(db, '/users'), orderByChild('uid'), equalTo(uid)));
      if (!snapshot.exists()) {
        throw new Error(`User with ${uid} not found!`);
      }

      const user = snapshot.val()[Object.keys(snapshot.val())[0]];
  

      return user;
  }