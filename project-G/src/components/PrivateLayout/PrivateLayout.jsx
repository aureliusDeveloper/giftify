import React from 'react';
import Section from '../UI/Section/Section';
import './PrivateLayout.css';
import { Outlet, useNavigate } from 'react-router';
import PrivateHeader from '../PrivateHeader/PrivateHeader';
const PrivateLayout = () => {
  const navigate = useNavigate();
  return (
    <Section>
      <PrivateHeader />
      <Outlet />
    </Section>
  );
};

export default PrivateLayout;
