import React from 'react';
import { useNavigate } from 'react-router-dom';
import './PublicHeader.css';
import logo from '../../assets/giftify-logo.png';
import NavigationLargeScreens from './Navigation/NavigationLargeScreens';
import NavigationSmallScreens from './Navigation/NavigationSmallScreens';


const PublicHeader = () => {
  const navigate = useNavigate();

  return (
    <header className="public-header">
      <img
        role="button"
        src={logo}
        alt="blue-giftify-logo"
        className="logo"
        onClick={() => navigate('/')}
      />

      <h2 className="public-header-title">
        <span className="public-header-span">GIFtify</span> the best place to
        browse GIFs!
      </h2>
      <NavigationLargeScreens/>
      <NavigationSmallScreens/>
    </header>
  );
};

export default PublicHeader;
