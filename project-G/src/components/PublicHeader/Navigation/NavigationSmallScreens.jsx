import React from 'react'
import { useState } from 'react'
import burgerMenu from '../../../assets/menu-burger-horizontal.svg';
import BurgerMenu from '../../BurgerMenu/BurgerMenu';
const NavigationSmallScreens = () => {
    const [showNav, setShowNav] = useState(false);
    const navHandler = () => setShowNav(showNav => !showNav);

  return (
    <div>
        <img role="button" src={burgerMenu} alt="burger-menu-icon"  className='burger-menu-icon' onClick={navHandler}/>
        {showNav && <BurgerMenu />}
    </div>
  )
}

export default NavigationSmallScreens