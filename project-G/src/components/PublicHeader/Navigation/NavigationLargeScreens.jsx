import React from 'react'
import { useNavigate } from 'react-router'
import Button from '../../UI/Button/Button'


const NavigationLargeScreens = () => {
    const navigate = useNavigate();
  return (
    <nav className='public-nav'>
    <Button type="button" title="Log In" handler={() => navigate('/login')}></Button>
    <Button type="button" title="Sign Up" handler={() => navigate('/register')}></Button>
</nav>
  )
}

export default NavigationLargeScreens