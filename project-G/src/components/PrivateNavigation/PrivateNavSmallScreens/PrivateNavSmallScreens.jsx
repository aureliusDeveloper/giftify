import React from 'react'
import { useNavigate } from 'react-router';
import { PRIVATE_HEADER_NAV } from '../../../common/enums';
import { logoutUser } from '../../../services/auth.service';
import './PrivateNavSmallScreens.css'


const PrivateNavSmallScreens = () => {

  const navigate = useNavigate();
  return (
    <div className='private-burger-menu__container'>
        <button className='private-btn-burger' onClick={() => navigate(PRIVATE_HEADER_NAV.routes.favorites)}>{PRIVATE_HEADER_NAV.title.favorites}</button>
        <button className='private-btn-burger' onClick={() => navigate(PRIVATE_HEADER_NAV.routes.upload)}>{PRIVATE_HEADER_NAV.title.upload}</button>
        <button className='private-btn-burger' onClick={logoutUser}>Logout</button>
    </div>
  )
}

export default PrivateNavSmallScreens