import React from 'react';
import { useNavigate } from 'react-router';
import { PRIVATE_HEADER_NAV, themeEnum } from '../../common/enums';
import { logoutUser } from '../../services/auth.service';
import Button from '../UI/Button/Button';
import './PrivateNavigation.css';
import sun from '../../assets/sun.svg';
import moon from '../../assets/moon.svg';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import { themeActions } from '../../store/theme';


export const PrivateNavigation = () => {
  const theme = useSelector(state => state.theme.theme);
  const dispatch = useDispatch();
 
  const navigate = useNavigate();
  useEffect(() => {
    const element = document.body;
    element.className = theme;
  }, [theme]);



  const toggleThemeHandler = () => {
    dispatch(themeActions.toggleTheme());
  };

  return (
    <nav className="private-nav">
      <Button
        type={PRIVATE_HEADER_NAV.type}
        title={PRIVATE_HEADER_NAV.title.home}
        handler={() => navigate(PRIVATE_HEADER_NAV.routes.home)}
      ></Button>
      <Button
        type={PRIVATE_HEADER_NAV.type}
        title={PRIVATE_HEADER_NAV.title.favorites}
        handler={() => navigate(PRIVATE_HEADER_NAV.routes.favorites)}
      ></Button>
      <Button
        type={PRIVATE_HEADER_NAV.type}
        title={PRIVATE_HEADER_NAV.title.upload}
        handler={() => navigate(PRIVATE_HEADER_NAV.routes.upload)}
      ></Button>
      <Button
        type={PRIVATE_HEADER_NAV.type}
        title={PRIVATE_HEADER_NAV.title.something}
        handler={logoutUser}
      ></Button>
      <div className="theme-container">
        <img
          role="button"
          src={theme === themeEnum.light ? moon : sun}
          alt="Sun or moon depending on the theme of the browser"
          onClick={toggleThemeHandler}
        />
      </div>
    </nav>
  );
};
