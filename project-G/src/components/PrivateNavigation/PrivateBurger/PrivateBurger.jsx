import React from 'react';
import { useState } from 'react';
import './PrivateBurger.css';
import burgerMenu from '../../../assets/menu-burger-horizontal.svg';
import burgerMenuWhite from '../../../assets/menu-burger-horizontal-white.svg';
import PrivateNavSmallScreens from '../PrivateNavSmallScreens/PrivateNavSmallScreens';
import { useSelector } from 'react-redux';
import { themeEnum } from '../../../common/enums';

const PrivateBurger = () => {
  const [showNav, setShowNav] = useState(false);
  const theme = useSelector((state) => state.theme.theme);

  const navHandler = () => setShowNav((showNav) => !showNav);

  return (
    <div>
      <img
        role="button"
        src={theme === themeEnum.dark ? burgerMenuWhite : burgerMenu}
        alt="burger-menu-icon"
        className="private-burger-menu-icon"
        onClick={navHandler}
      />
      {showNav && <PrivateNavSmallScreens />}
    </div>
  );
};

export default PrivateBurger;
