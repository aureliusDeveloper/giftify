import React from 'react';
import { Navigate, Outlet } from 'react-router';
import PublicHeader from '../PublicHeader/PublicHeader';
import LoadingSpinner from '../UI/LoadingSpinner/LoadingSpinner';
import Section from '../UI/Section/Section';
import './PublicLayout.css';

const PublicLayout = ({ loading, user }) => {
  return loading ? (
    <LoadingSpinner />
  ) : user !== null ? (
    <Navigate to={'/private'} />
  ) : (
    <Section>
      <PublicHeader />
      <Outlet />
    </Section>
  );
};

export default PublicLayout;
