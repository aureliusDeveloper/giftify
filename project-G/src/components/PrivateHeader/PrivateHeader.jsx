import React from 'react'
import './PrivateHeader.css'
import logo from '../../assets/giftify-logo.png';
import { useNavigate } from 'react-router';
import { PrivateNavigation } from '../PrivateNavigation/PrivateNavigation';
import PrivateBurger from '../PrivateNavigation/PrivateBurger/PrivateBurger';
import { useSelector } from 'react-redux';
import { themeEnum } from '../../common/enums';
import sun from '../../assets/sun.svg';
import moon from '../../assets/moon.svg';
import { useDispatch } from 'react-redux';
import { themeActions } from '../../store/theme';

const PrivateHeader = () => {
    const navigate = useNavigate();
    const userData = useSelector(state => state.user.userData);

    const dispatch = useDispatch();
    const theme = useSelector(state => state.theme.theme);
    const toggleThemeHandler = () => {
      dispatch(themeActions.toggleTheme());
    };
  
  
  return (
    <header className="private-header">
      <div className='private-header-theme'>
      <img
          role="button"
          src={theme === themeEnum.light ? moon : sun}
          alt="Sun or moon depending on the theme of the browser"
          onClick={toggleThemeHandler}
        />
      </div>
    <img
      role="button"
      src={logo}
      alt="blue-giftify-logo"
      className="logo"
      onClick={() => navigate('/private')}
    />
     <h2 className="private-header-title">
        <span className="private-header-span">WELCOME {userData?.username}</span> hope you have great time here!
      </h2>
      <PrivateNavigation />
      <PrivateBurger />
  </header>
  )
}

export default PrivateHeader