import React from 'react'
import { useNavigate } from 'react-router'
import './BurgerMenu.css'

const BurgerMenu = () => {
    const navigate = useNavigate();
  return (
    <div className='burger-menu__container'>
        <button className='btn-burger' onClick={() => navigate('/login')}>Log In</button>
        <button className='btn-burger' onClick={() => navigate('/register')}>Sign Up</button>
    </div>
  )
}

export default BurgerMenu