import React from 'react';
import './ShareModal.css';
import Button from '../UI/Button/Button';
import copyLinkIcon from '../../assets/copy-link.svg';

const ShareModal = ({ toggleModalHandler, url, copied, handleCopyUrl, copiedError }) => {
  return (
    <div className="modal-container">
      <header>
        <h2 className="modal-title">Share GIF</h2>
      </header>
      <div className="modal-url">
        <label htmlFor="gif-url">Copy URL:</label>
        <input type="text" name="gif-url" className='url-input' placeholder={url} onClick={() => handleCopyUrl(url)}></input>
        <div className="modal-copy">
          <img src={copyLinkIcon} alt="Copy link icon" onClick={() => handleCopyUrl(url)}/>
        </div>
        {copied && <p className='url-copied'>URL was copied successfully!</p>}
        {copiedError.flag && <p className='url-not-copied'>{copiedError.message}</p>}
      </div>
      <footer>
        <Button
          buttonType="button"
          title="Close"
          handler={toggleModalHandler}
        />
      </footer>
    </div>
  );
};

export default ShareModal;
