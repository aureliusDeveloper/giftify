import { forwardRef } from 'react'
import './GifCard.css'

const GifCard = forwardRef(({props},ref) => {

    return (
        <div className='gif-card__container' ref={ref}>
                <h2 className='gif-card__title'>{props.title}</h2>
               <div className='gif-card-image__wrapper'>
              <img src={props?.images?.downsized?.url} alt={props.title}/>
               </div>
           </div>
         )
});


export default GifCard