import React from 'react';
import { useEffect } from 'react';
import { forwardRef } from 'react';
import { useState } from 'react';
import { useSelector } from 'react-redux';
import { getFavoriteGif, toggleLike } from '../../../services/gifs.service';
import ShareModal from '../../ShareModal/ShareModal';
import './PrivateGifCard.css';

const PrivateGifCard = forwardRef((props, ref) => {

  const [isFavorite, setIsFavorite] = useState('');
  const [openModal, setOpenModal] = useState(false);
  const [copied, setCopied] = useState(false);
  const [copiedError, setCopiedError] = useState({
    flag: false,
    message: '',
  });

  const userData = useSelector((state) => state.user.userData);

  if (openModal) {
    document.body.classList.add('no-scroll');
  } else {
    document.body.classList.remove('no-scroll');
  }

  useEffect(() => {
    getFavoriteGif(props.gif.id, userData?.username).then(setIsFavorite);
  }, [userData?.username, isFavorite]);

  const favoriteHandler = async () => {
    const result = await toggleLike(props.gif.id, userData?.username);

    if (result === true) {
      setIsFavorite(props.gif.id);
    } else {
      setIsFavorite('');
    }
    props.setIsToggled((isToggled) => !isToggled);
  };

  const toggleModalHandler = () => {
    setOpenModal((openModal) => !openModal);
    setCopied(false);
    setCopiedError(({
      flag: false,
      message: '',
    }));
  };

  const handleCopyUrl = async (url) => {
    try {
      await navigator.clipboard.writeText(url);
      setCopied(true);
    } catch (error) {
      setCopiedError({
        flag: true,
        message: error.message,
      });
    }
  };

  return (
    <>
      {openModal && (
        <div className="overlay" onClick={toggleModalHandler}></div>
      )}
      {openModal && (
        <ShareModal
          toggleModalHandler={toggleModalHandler}
          url={props.gif?.images?.downsized_large?.url}
          copied={copied}
          handleCopyUrl={handleCopyUrl}
          copiedError={copiedError}
        />
      )}
      <div className="private-gif-card__container" ref={ref}>
        <h3 className="private-gif-card__title">{props.gif.title}</h3>
        <div className="private-gif-card__image">
          <img src={props.gif?.images?.downsized?.url} alt={props.gif.title} />
        </div>
        <div className="private-gif-card__wrapper">
          <div className="private-gif-card__btns">
            <button
              className="fa fa-share fav-btn fav-btn-share"
              style={{ fontSize: '0px' }}
              onClick={toggleModalHandler}
            ></button>
            <button
              className="fa fa-heart fav-btn"
              style={{
                fontSize: '0px',
                color: `${isFavorite === props.gif.id ? 'red' : '#fff'}`,
              }}
              onClick={favoriteHandler}
            ></button>
          </div>
        </div>
      </div>
    </>
  );
});

export default PrivateGifCard;
