import React from 'react'
import { useSelector } from 'react-redux'
import { themeEnum } from '../../../common/enums';
import './Button.css'

const Button = ({buttonType, title, handler}) => {
  const theme = useSelector((state) => state.theme.theme);
  const currentActiveButton = useSelector((state) => state.category.category);

  return (
    <button type={buttonType} className={`btn-primary ${theme === themeEnum.dark && `btn-primary-dark`} ${currentActiveButton === title && 'btn-active'}`} onClick={handler}>{title}</button>
  )
}

export default Button