import React from 'react'
import './Section.css'

const Section = ({children, additionalClass}) => {
  return (
    <div className={`section ${additionalClass || ''}`}>{children}</div>
  )
}
export default Section