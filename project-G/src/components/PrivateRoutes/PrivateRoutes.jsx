import React from 'react'
import { Navigate, Outlet } from 'react-router';
import LoadingSpinner from '../../components/UI/LoadingSpinner/LoadingSpinner'

const PrivateRoutes = ({loading, user}) => {
  return (
    loading ? <LoadingSpinner /> : user !== null ? <Outlet/> : <Navigate to={'/login'}/>
  )
}

export default PrivateRoutes