// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import {getAuth} from 'firebase/auth';
import {getDatabase} from 'firebase/database';
import {getStorage} from 'firebase/storage';

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBrPz801xYOIoW9ZWm3W3TuC_KpT6zhkgU",
  authDomain: "giftify-2648b.firebaseapp.com",
  databaseURL: "https://giftify-2648b-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "giftify-2648b",
  storageBucket: "giftify-2648b.appspot.com",
  messagingSenderId: "761591345490",
  appId: "1:761591345490:web:44f45de7a7c2ca51158a8b",
  databaseURL: "https://giftify-2648b-default-rtdb.europe-west1.firebasedatabase.app/",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);
export const db = getDatabase(app);
export const storage = getStorage(app);
