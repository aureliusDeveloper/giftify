import React from 'react';
import './PrivateHome.css';
import Button from '../../components/UI/Button/Button';
import { categories } from '../../common/enums';
import { useEffect } from 'react';
import { useState } from 'react';
import PrivateGifCard from '../../components/UI/PrivateGifCard/PrivateGifCard';
import { v4 } from 'uuid';
import useSearchGifs from '../../hooks/useSearchGifs';
import { SEARCH_PAGE_SIZE } from '../../common/constants';
import LoadingSpinner from '../../components/UI/LoadingSpinner/LoadingSpinner';
import { useRef } from 'react';
import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { categoryButtonsActions } from '../../store/categories';



const PrivateHome = () => {
  const [query, setQuery] = useState(categories.trending);
  const [searchParam, setSearchParam] = useState('');
  const [isToggled, setIsToggled] = useState(false);
  const [page, setPage] = useState(0);

  const dispatch = useDispatch();

  const { searchedGifs, hasMore, loading, error} = useSearchGifs(query, page, SEARCH_PAGE_SIZE);

  const observer = useRef();
  const lastGifReference = useCallback(
    (node) => {
      if (loading) return;
      if (observer.current) observer.current.disconnect();
      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting && hasMore) {
          setPage((prevPage) => prevPage + 1);
        }
      });
      if (node) observer.current.observe(node);
    },
    [loading, hasMore]
  );


  useEffect(() => {
    const delayDebounceFn = setTimeout(() => {
      if (searchParam !== '') {
        setQuery(searchParam);
        dispatch(categoryButtonsActions.setActive(''));
      } else {
        setQuery(categories.trending);
        dispatch(categoryButtonsActions.setActive(categories.trending));
      }
    }, 2000)

    return () => clearTimeout(delayDebounceFn)

  }, [searchParam]);



  return (
    <div className="private-container">
      <nav className='private-home__nav'>
      <Button type='button' title={categories.trending} handler={() => {
        setQuery(categories.trending);
        dispatch(categoryButtonsActions.setActive(categories.trending));
      }}/>
      <Button type='button' title={categories.sport} handler={() => {
        setQuery(categories.sport);
        dispatch(categoryButtonsActions.setActive(categories.sport));
      }}/>
      <Button type='button'  title={categories.animals} handler={() => {
        setQuery(categories.animals);
        dispatch(categoryButtonsActions.setActive(categories.animals));
      }}/>
      <Button type='button' title={categories.fun} handler={() => {
        setQuery(categories.fun);
        dispatch(categoryButtonsActions.setActive(categories.fun));
      }}/>
      <Button type='button' title={categories.love} handler={() => {
        setQuery(categories.love);
        dispatch(categoryButtonsActions.setActive(categories.love));
      }}/>
      <Button type='button' title={categories.music} handler={() => {
        setQuery(categories.music);
        dispatch(categoryButtonsActions.setActive(categories.music));
      }}/>
      <Button type='button' title={categories.recent} handler={() => {
        setQuery(categories.recent);
        dispatch(categoryButtonsActions.setActive(categories.recent));
      }}/>
        <input type='text' placeholder='Search...' value={searchParam} onChange={(e) => setSearchParam(e.target.value)}/>
      </nav>
      <div className="private-home__content">
        {searchedGifs.length > 0 ? searchedGifs.map((gif, idx) => {
          if (searchedGifs.length === idx + 1) {
           return <PrivateGifCard key={v4()} gif={gif} setIsToggled={setIsToggled} ref={lastGifReference}/>
          } else {
           return <PrivateGifCard key={v4()} gif={gif} setIsToggled={setIsToggled}/>
          }
        }): <h1 className='no-gifs__message'>Oops.. There are no GIFs with {query}</h1>}
      </div>
      <div>{loading === true ? <LoadingSpinner /> : null}</div>
      <div>{error && 'Oops... Something went wrong!'}</div>
    </div>
  );
};

export default PrivateHome;
