import React from 'react'
import './Login.css'
import Section from '../../components/UI/Section/Section'
import { Link, useNavigate } from 'react-router-dom'
import { useState } from 'react'
import Button from '../../components/UI/Button/Button'
import { loginUser } from '../../services/auth.service'

const Login = () => {
  const [wrongCredentials, setWrongCredentials] = useState(false);
  const [form, setForm] = useState({
    email: {
      value: '',
      touched: false,
      valid: false,
      error: '',
    },
    username: {
      value: '',
      touched: false,
      valid: false,
      error: '',
    },
    password: {
      value: '',
      touched: false,
      valid: false,
      error: '',
    },
  });


  const navigate = useNavigate();

  const updateEmail = (value = '') => {
    // valid email address should include @
    setForm({
      ...form,
      email: {
        value,
        touched: true,
        valid: value.includes('@'),
        error: !value.includes('@') && 'Please type valid email address!',
      },
    });
  };

  const updatePassword = (value = '') => {
 
    setForm({
      ...form,
      password: {
        value,
        touched: true,
        valid: value.length >= 6 && value.length <= 60,
        error:
          value.length < 6
            ? 'Minimum password length: 6'
            : 'Maximum password length: 60',
      },
    });
  };

  const submitHandler = async (e) => {
    e.preventDefault();
    if (!form.email.value || !form.password.value) {
      return;
    }

    try {
       await loginUser(form.email.value, form.password.value);
        navigate('/private');
      setForm({
        email: {
          value: '',
          touched: false,
          valid: false,
          error: '',
        },
        password: {
          value: '',
          touched: false,
          valid: false,
          error: '',
        },
      });
    } catch (error) {

      if (error.message.includes('auth/wrong-password')) {
        setWrongCredentials(true);
      }
      console.log(error.message);
    }
  };


  return (
    <Section additionalClass='section-buff__login'>
        <header className="login-description">
        <h1>
          Entertainment, excitement and fun is what expects you here!
        </h1>
      </header>
      <form className="login-form" onSubmit={submitHandler}>
        <header className="login-header">
          <h2>Log In</h2>
          <p>Welcome again, hope you enjoy your time here!</p>
        </header>
        <div className="login-fields">
          <label htmlFor="email">Email</label>
          <input
            type="email"
            name="email"
            value={form.email.value}
            onChange={(e) => updateEmail(e.target.value)}
            placeholder="Email here..."
          />
          <label>
            {form.email.touched && !form.email.valid && (
              <span className="error">{form.email.error}</span>
            )}
          </label>
        </div>
        <div className="login-fields">
          <label htmlFor="password">Password</label>
          <input
            type="password"
            name="password"
            value={form.password.value}
            onChange={(e) => updatePassword(e.target.value)}
            placeholder="Password here..."
          />
          <label>
            {form.password.touched && !form.password.valid && (
              <span className="error">{form.password.error}</span>
            )}
          </label>
        </div>

        <div className="login-buttons">
          <Button type="button" title="Cancel" handler={() => navigate('/')} />
          <Button title="Log In" />
        </div>
        {wrongCredentials && (
          <span className="error">
           Wrong password!
          </span>
        )}
        <strong>
          Not registered yet? <Link to="/register">click here to register</Link>
        </strong>
      </form>
    </Section>
  )
}

export default Login