import React from 'react';
import { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import Button from '../../components/UI/Button/Button';
import Section from '../../components/UI/Section/Section';
import { registerUser } from '../../services/auth.service';
import { createUser } from '../../services/users.service';
import './Register.css';

const Register = () => {
  const [form, setForm] = useState({
    email: {
      value: '',
      touched: false,
      valid: false,
      error: '',
    },
    username: {
      value: '',
      touched: false,
      valid: false,
      error: '',
    },
    password: {
      value: '',
      touched: false,
      valid: false,
      error: '',
    },
  });


  const [isUsed, setIsUsed] = useState(false);

  const navigate = useNavigate();


  const updateEmail = (value = '') => {
    // valid email address should include @
    setForm({
      ...form,
      email: {
        value,
        touched: true,
        valid: value.includes('@'),
        error: !value.includes('@') && 'Please type valid email address!',
      },
    });
  };

  const updateUsername = (value = '') => {
    // username between 4 and 20

    setForm({
      ...form,
      username: {
        value,
        touched: true,
        valid: value.length >= 4 && value.length <= 20,
        error:
          value.length < 4
            ? 'Minimum username length: 4'
            : 'Maximum username length: 20',
      },
    });
  };

  const updatePassword = (value = '') => {
    // username between 10 and 60

    setForm({
      ...form,
      password: {
        value,
        touched: true,
        valid: value.length >= 6 && value.length <= 60,
        error:
          value.length < 6
            ? 'Minimum password length: 6'
            : 'Maximum password length: 60',
      },
    });
  };

  const submitHandler = async (e) => {
    e.preventDefault();
    if (!form.email.value || !form.password.value) {
      return;
    }

    try {
  
      const credentials = await registerUser(
        form.email.value,
        form.password.value
      );
    

      try {
        
        await createUser(
          credentials.user.uid,
          form.username.value,
          form.email.value
        );
      
      } catch (error) {
        console.log(error.message);
      }

      setForm({
        email: {
          value: '',
          touched: false,
          valid: false,
          error: '',
        },
        username: {
          value: '',
          touched: false,
          valid: false,
          error: '',
        },
        password: {
          value: '',
          touched: false,
          valid: false,
          error: '',
        },
      });
    } catch (error) {
      if (error.message.includes('auth/email-already-in-use')) {
        setIsUsed(true);
      }
      console.log(error.message);
    }
  };


  return (
    <Section additionalClass="section-buff__register">
      <header className="register-description">
        <h1>
          Browse the funniest and the most epic GIFs from around the world!
        </h1>
      </header>
      <form className="register-form" onSubmit={submitHandler}>
        <header className="register-header">
          <h2>Sign Up</h2>
          <p>Get ready to be entertained with the most amazing GIFs!</p>
        </header>
        <div className="register-fields">
          <label htmlFor="email">Email</label>
          <input
            type="email"
            name="email"
            value={form.email.value}
            onChange={(e) => updateEmail(e.target.value)}
            placeholder="Email here..."
          />
          <label>
            {form.email.touched && !form.email.valid && (
              <span className="error">{form.email.error}</span>
            )}
          </label>
        </div>
        <div className="register-fields">
          <label htmlFor="username">Username</label>
          <input
            type="text"
            name="username"
            value={form.username.value}
            onChange={(e) => updateUsername(e.target.value)}
            placeholder="Username here..."
          />
          <label>
            {form.username.touched && !form.username.valid && (
              <span className="error">{form.username.error}</span>
            )}
          </label>
        </div>
        <div className="register-fields">
          <label htmlFor="password">Password</label>
          <input
            type="password"
            name="password"
            value={form.password.value}
            onChange={(e) => updatePassword(e.target.value)}
            placeholder="Password here..."
          />
          <label>
            {form.password.touched && !form.password.valid && (
              <span className="error">{form.password.error}</span>
            )}
          </label>
        </div>

        <div className="register-buttons">
          <Button type="button" title="Cancel" handler={() => navigate('/')} />
          <Button type="submit" title="Sign Up" />
        </div>
        {isUsed && <span className="error">Email already used!</span>}
        <strong>
          Already have an account? <Link to="/login">click here to login</Link>
        </strong>
      </form>
    </Section>
  );
};

export default Register;
