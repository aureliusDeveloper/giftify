import React, { useEffect } from 'react';
import { useCallback, useRef, useState } from 'react';
import { Link } from 'react-router-dom';
import { v4 } from 'uuid';
import { TRENDING_PAGE_SIZE } from '../../common/constants';
import GifCard from '../../components/UI/GifCard/GifCard';
import LoadingSpinner from '../../components/UI/LoadingSpinner/LoadingSpinner';
import useLoadMoreGifs from '../../hooks/useLoadMoreGifs';
import './PublicHome.css';

const PublicHome = () => {
  const [page, setPage] = useState(0);
  const { trendingGifs, hasMore, loading, error } = useLoadMoreGifs(
    page,
    TRENDING_PAGE_SIZE
  );

  const observer = useRef();
  const lastGifReference = useCallback(
    (node) => {
      if (loading) return;
      if (observer.current) observer.current.disconnect();
      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting && hasMore) {
          setPage((prevPage) => prevPage + 1);
        }
      });
      if (node) observer.current.observe(node);
    },
    [loading, hasMore]
  );

  return (
    <div className='public-home-wrapper'>
        <h1 className='public-home__title'>Here are the trending gifs right now! Do you want to share them with friends and see more?</h1>
          <Link to="/login" className='start-now__button'>Start now</Link>
      <div className="gifs-container">
        {trendingGifs.map((gif, idx) => {
          if (trendingGifs.length === idx + 1) {
            return <GifCard key={v4()} props={gif} ref={lastGifReference} />;
          } else {
            return <GifCard key={v4()} props={gif} />;
          }
        })}
      </div>
      <div>{loading === true ? <LoadingSpinner /> : null}</div>
      <div>{error && 'Oops... Something went wrong!'}</div>
    </div>
  );
};

export default PublicHome;
